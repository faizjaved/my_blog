from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Max
from django.db import IntegrityError
from django.shortcuts import render, redirect
from .models import Question, Answer
from .forms import UserForm
from django.shortcuts import HttpResponse
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User
from datetime import datetime, timedelta


def index(request):
    questions = Question.objects.all()
    return render(request,'dashboard/index.html', {'questions': questions})


@login_required
def ask_question(request):
    user = request.user
    print(request.user)
    if request.POST:
        question_asked = request.POST.get('question')
        question = Question()
        question.question_text = question_asked
        question.user = user
        question.save()
        return redirect('/')
    else:
        return render(request, 'dashboard/ask_questions.html')


@login_required
def answer(request, question_id):
    question = Question.objects.get(pk=question_id)
    user = request.user

    if request.POST:
        submitted_answer = request.POST.get('answer')
        answer_values = Answer()
        answer_values.question = question
        answer_values.answer_text = submitted_answer
        try:
            answer_values.save()
        except IntegrityError:
            return redirect('/')

    else:
        return render(request, 'dashboard/answer.html', {'question': question})
    return redirect('/')


@login_required
def profile(request):
    user = request.user
    answers_written = []
    questions = Question.objects.filter(user=user)
    for question in questions:
        answers = Answer.objects.filter(question=question).first()
        answers_written.append(answers)
    return render(request, 'dashboard/profile.html', {'user': user, 'questions': questions, 'answers': answers_written})


def register(request):
    if request.POST:
        user_form = UserForm(request.POST)
        if user_form.is_valid():
            user_name = user_form.cleaned_data['username']
            pass_word = user_form.cleaned_data['password']
            email = user_form.cleaned_data['email']
            user = User.objects.create_user(user_name, email, pass_word)
            user.save()
            user.first_name = user_form.cleaned_data['first_name']
            user.last_name = user_form.cleaned_data['last_name']
            user.save()
            return redirect('dashboard:login')
    else:
        user_form = UserForm()
    return render(request, 'dashboard/register.html', {'user_form': user_form})


def login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username,
                            password=password)
        if user.is_authenticated:
            if user is not None and user.is_active:
                auth_login(request, user)
                return redirect('dashboard:index')
        return HttpResponse("Login Failed. Invalid username or password")
    else:
        return render(request, "dashboard/login.html")


def stats(request):
    hour_ago = datetime.now() - timedelta(hours=1)
    upvoted_answer = Answer.objects.filter(up_votes__gt=0, updated_on__gt=hour_ago).order_by('-up_votes')
    max = Answer.objects.all().aggregate(Max('up_votes'))
    upvote_max = max['up_votes__max']
    max_upvoted_answer = Answer.objects.filter(up_votes__exact=upvote_max)
    # print(max_upvoted_answer)
    return render(request, 'dashboard/stats.html', {'upvoted_last_hour': upvoted_answer, 'max_upvoted_answers': max_upvoted_answer})


@login_required
def upvote(request, question_id):
    answer = Answer.objects.get(question=question_id)
    answer.up_votes += 1
    answer.save()
    return redirect('/')


def logout(request):
    auth_logout(request)
    return redirect('dashboard:login')