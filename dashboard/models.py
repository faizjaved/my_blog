from django.db import models
from django.contrib.auth.models import User


class Question(models.Model):
    user = models.ForeignKey(User)
    question_text = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.question_text

    class Meta:
        db_table = 'question'


class Answer(models.Model):
    question = models.OneToOneField(Question, on_delete=models.CASCADE, related_name='question')
    answer_text = models.TextField()
    up_votes = models.IntegerField(default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.answer_text

    class Meta:
        db_table = 'answer'
