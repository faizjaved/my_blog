from django.conf.urls import url
from . import views

app_name = 'dashboard'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^ask-question$', views.ask_question, name='ask_question'),
    url(r'^answer/(?P<question_id>\d+)$', views.answer, name='answer'),
    url(r'^logout', views.logout, name='logout'),
    url(r'^answer/upvote/(?P<question_id>\d+)$', views.upvote, name='upvote'),
    url(r'^register$', views.register, name='register'),
    url(r'^login$', views.login, name='login'),
    url(r'^profile$', views.profile, name='profile'),
    url(r'^stats$', views.stats, name='stats')
]
