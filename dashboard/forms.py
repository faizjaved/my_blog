
from django.contrib.auth.models import User
from django import forms
from django.forms import ModelForm


class UserForm(ModelForm):
    class Meta:
        model = User
        widgets = {
            'password': forms.PasswordInput(),

        }
        fields = ['first_name', 'last_name', 'username', 'email', 'password']
